require File.dirname( __FILE__ ) + '/ServiceProvider'

require 'Qt4'

module RPProj

    # Service meta class.
    #
    # The ServiceMeta class is an object adapter for the ServiceProvider class.
    # It holds additional data about the service (as displayable name).
    # Additionally, this class provides some additional functionality e.g. for
    # loading and saving settings.
    class ServiceMeta < ServiceProvider

        attr_accessor :service
        attr_accessor :name

        # Constructor.
        #
        # Creates a new ServiceMeta object.
        # It takes two parameters with default values:
        # First, the service is an ServiceProvider instance.
        # That can be set later, too.
        #
        # Parent is the parent object for this object.
        def initialize( service = nil, parent = nil )
            super( parent )

            @service = service
            connectToService

            @name = ""
            @issues = []

            # for backing up underlying service data, if
            # provider is teporarily not loadable
            @serviceBackupData = Qt::ByteArray.new
            @serviceType = ""
        end

        # The service's name.
        #
        # Returns the underlying service's name.
        def name
            return @name
        end

        # Configure the underlying service.
        #
        # Configures the underlying service, i.e. mostly shows the service's
        # configuration dialog.
        def configure
            if not @service.nil? then
                return @service.configure
            end
            return false
        end

        # Save settings.
        #
        # Saves the settings of this service meta object to a Qt::ByteArray and
        # returns it.
        def saveSettings
            result = Qt::ByteArray.new
            stream = Qt::DataStream.new( result, Qt::IODevice.WriteOnly )
            stream << @name
            if not @service.nil? then
                stream << @service.class.name << @service.saveSettings
            else
                stream << @serviceType << @serviceBackupData
            end
            stream << "#{@issues.length}"
            @issues.each do |issue|
                stream << issue.saveSettings
            end
            return result
        end

        # Restore settings.
        #
        # Load the settings from a Qt::ByteArray as returned by saveSettings.
        def restoreSettings( data )
            stream = Qt::DataStream.new( data, Qt::IODevice.ReadOnly )
            @name = ""
            stream >> @name
            @serviceType = ""
            @serviceBackupData = Qt::ByteArray.new
            stream >> @serviceType >> @serviceBackupData
            serviceClass = ServiceProvider.service( @serviceType )
            if not serviceClass.nil? then
                @service = serviceClass.new( self )
                connectToService
                @service.restoreSettings( @serviceBackupData )
            end
            numIssues = ""
            stream >> numIssues
            numIssues = numIssues.to_i
            @issues = []
            while numIssues > 0 do
                issue = IssueMeta.new
                issueData = Qt::ByteArray.new
                stream >> issueData
                issue.restoreSettings( issueData )
                @issues.push( issue )
                numIssues -= 1
            end
        end

        # Update the service.
        #
        # Updates the underlying service.
        def update
            if not @service.nil? then
                @service.update
            end
        end

        # List of issues.
        #
        # Returns the list of IssueMeta objects. These encapsulate the
        # issues from the underlying service.
        def issues
            return @issues
        end

        # Getters and setters
        def service=( service )
            @service = service
            connectToService
        end

        private

        # Connect to the signals provided by the underlying service (if any is set)
        def connectToService
            if not service.nil? then
                service.connect( SIGNAL('issueListUpdated()') ) { updateMyIssues }
            end
        end

        # Update the issue list.
        #
        # This updates the internal issue list to the underlying service's one.
        # Basically, this sets the issues of the meta issues and creates new
        # meta issues for issues, that does not already have one.
        # Additionally, this emits the issueListUpdated signal to
        # let depending clients know that something changed in the list.
        def updateMyIssues
            if not @service.nil? then
                @service.issues.each do |issue|
                    myIssue = issueById( issue.id )
                    if myIssue.nil? then
                        newIssue = IssueMeta.new( issue )
                        @issues << newIssue
                    else
                        myIssue.issue = issue
                    end
                end
            end
            emit issueListUpdated()
        end

        # Returns an issue by its ID
        #
        # Searches the list of Meta Issues for one with the giben ID.
        # If no such issue is found, nil is returned.
        def issueById( id )
            @issues.each do |issue|
                if issue.id == id then
                    return issue
                end
            end
            return nil
        end

    end

end