require File.dirname( __FILE__) + 'Issue'

require 'Qt4'

module RPProj

    # Issue Meta information
    #
    # The issue meta class is used to store meta information about issues.
    # Basically, the class is an object adapter to the Issue class. It shares
    # the same interface, but usually returns the stored issue's values.
    #
    # Additionally, this class allows to store meta information (i.e.
    # information that is not stored where the issue is fetched from).
    #
    # Also, this class is used to cache the issue data.
    # So, once issue data is fetched, this class holds it so the issue can be
    # displayed even if the service is temporarily not available.
    class IssueMeta < Issue

        attr_accessor :issue

        attr_reader :id
        attr_reader :subject
        attr_reader :isNew
        attr_reader :properties

        attr_accessor :comment

        # Constructor.
        #
        # Takes an optional issue.
        # The issue can be set later, too.
        def initialize( issue = nil )
            @issue = issue
            @id = ""
            @subject = ""
            @isNew = false
            @properties = {}
        end

        # Returns the underlying issue's ID.
        def id
            if not @issue.nil? then
                @id = @issue.id
            end
            return @id
        end

        # Returns the underlying issue's subject.
        def subject
            if not @issue.nil? then
                @subject = @issue.subject
            end
            return @subject
        end

        # Returns, whether the underlying issue is read.
        def isNew
            if not @issue.nil? then
                @isNew = @issue.isNew
            end
            return @isNew
        end

        # Returns the underlying issue's properties.
        def properties
            if not @issue.nil? then
                @properties = @issue.properties
            end
            return @properties
        end

        # Shows the underlying issue's "Details" dialog.
        def showDetails
            if not @issue.nil? then
                @issue.showDetails
            end
        end

        # Save issue meta settings
        #
        # Saves the currently hold data to a Qt::ByteArray and returns it.
        def saveSettings
            result = Qt::ByteArray.new
            stream = Qt::DataStream.new( result, Qt::IODevice::WriteOnly )
            stream << "#{@id}"
                   << "#{@subject}"
                   << "#{@isNew}"
                   << "#{@properties.length}"
            @properties.keys.each_pair do |key,value|
                stream << "#{key}"
                       << "#{value}"
            end
            stream << "#{@comment}"
            return result
        end

        # Restore issue meta settings.
        #
        # Loads data for this issue meta object from a Qt::ByteArray as returned
        # from saveSettings.
        def restoreSettings( data )
            clear( true )
            if data.nil? or data.empty? then
                return
            end
            stream = Qt::DataStream.new( data, Qt::IODevice::ReadOnly )
            isNew = ""
            stream >> @id
                   >> @subject
                   >> isNew
            @isNew = ( isNew == "true" ? true : false )
            numProperties = ""
            stream >> numProperties
            numProperties = numProperties.to_i
            while numProperties > 0 do
                key = ""
                value = ""
                stream >> key
                       >> value
                @properties[ key ] = value
                numProperties -= 1
            end
            stream >> @comment
        end

        # Clear the issue meta data.
        #
        # Removes stored data about the issue. When called
        # without parameters or with the default parameter value, this
        # will only clear the cached issue data.
        #
        # Passing true for the complete parameter will cause a complete
        # clear operation, i.e. also user provided meta information will be
        # deleted.
        def clear( complete = false )
            @id = ""
            @subject = ""
            @isNew = false
            @properties = {}

            if complete then
                @comment = ""
            end
        end

    end

end