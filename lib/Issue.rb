module RPProj

    # Base class for issues.
    #
    # Project adapters should specialize this class to
    # provide information and specialized behaviour.
    class Issue

        # The issue's ID
        #
        # The ID is used to identify the issue within the Service it belongs
        # to.
        def id
            return ""
        end
        
        # The issue's subject.
        #
        # The subject is displayed in listings and usually will
        # be the title of the issue.
        def subject
            return ""
        end

        # The issue status.
        #
        # This shall return true of the issue is new or updated.
        def isNew
            return false
        end
        
        # Extended properties of the issue.
        #
        # Returns a hash providing extended information about the issue.
        # This will be used e.g. for filtering.
        def properties
            return {}
        end

        # Show a dialog with details.
        #
        # This shall show a dialog to the user providing detailed information
        # about the issue.
        def showDetails
        end

    end

end