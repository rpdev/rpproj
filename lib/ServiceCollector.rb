require File.dirname( __FILE__ ) + '/ServiceMeta'

require 'Qt4'

module RPProj

    # Collects service instances.
    #
    # This class collects instances of the Service class. Additionally, it
    # provides methods for persisting the data when the application 
    # terminates.
    class ServiceCollector < Qt::Object


        signals :accountListUpdated
        
        attr_reader :services
        
        # Ctor
        def initialize( parent )
            super( parent )
            
            @services = []
        end
        
        # Save service collector.
        #
        # Writes the service collector to a Qt::ByteArray and returns it.
        # This is used when the application terminates. All instances of the 
        # ServiceProvider class (or subclasses) are written to a ByteArray, which in
        # turn can be saved to disk.
        def saveSettings
            result = Qt::ByteArray.new
            stream = Qt::DataStream.new( result, Qt::IODevice.WriteOnly )
            stream << "#{ @services.size }"
            @services.each do |service|
                stream << service.saveSettings
            end
            return result
        end
        
        # Restore the ServiceCollector. 
        #
        # Restores the collector from an Qt::ByteArray as written using saveSettings.
        def restoreSettings( data )
            @services = []
            stream = Qt::DataStream.new( data, Qt::IODevice::ReadOnly )
            num = ""
            stream >> num
            num = num.to_i
            while num > 0 do
                buffer = Qt::ByteArray.new
                stream >> buffer
                service = ServiceMeta.new( nil, self )
                service.connect( SIGNAL('issueListUpdated()') ) { emit accountListUpdated() }
                service.restoreSettings( buffer )
                @services << service
                num = num - 1
            end
            emit accountListUpdated()
        end

        # Add a new service.
        #
        # Adds a new service account to the end of the account list and emits
        # a signal to let clients know about the change.
        def addService( service )
            if not service.nil? then
                @services.push( service )
                service.connect( SIGNAL('issueListUpdated()') ) { emit accountListUpdated() }
                emit accountListUpdated
            end
        end
        
        # Remove a service from the collector.
        #
        # Deletes all occurrences of the service from the account list and emits
        # a signal to let clients know about the changed list.
        def removeService( service )
            keep = []
            @services.each do |s|
                if not ( s == service ) then
                    keep.push( s )
                end
            end
            @services = keep
            emit accountListUpdated()
        end

        private

        # An account's issue list has been changed in some way.
        #
        # This simply emits an accountListUpdated signal, so that
        # clients can update e.g. their views to reflect the changes (i.e
        # concerning new/unread issues and so forth).
        def onAccountIssueListChanged
            emit accountListUpdated()
        end
        
    end
  
end