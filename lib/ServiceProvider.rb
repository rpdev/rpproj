require 'Qt4'

require File.dirname( __FILE__ ) + "/Issue"

module RPProj

    # Base class for services and service manager.
    #
    # Plugins can specialize this class to provide access to
    # further backends. Additionally, this class provides methods
    # to manage all available plugins.
    class ServiceProvider < Qt::Object

        # The list of issues has been updated.
        #
        # This signal shall be emitted whenever the list of issues
        # has changed.
        signals :issueListUpdated
        
        # List of known services.
        @@services = {}

        # List of paths to search for providers
        @@providerSearchPaths = [ "#{File.dirname( __FILE__ )}/../providers/" ]

        # Register a new service.
        #
        # Register service in the list of known services; service is a
        # class, which will be identified by its class name. The displayName
        # is used when displaying the service list in some way to the user and
        # should be self-explanatory (e.g. Drupal Service for a Drupal
        # service provider).
        def self.registerService( service, displayName )
            @@services[ service ] = displayName
        end

        # Add a new provider search path
        #
        # Adds path to the beginning of the lift of paths searched for
        # providers.
        def self.registerProviderSearchPath( path )
            @@providerSearchPaths.unshift( path )
        end

        # Searches all registered paths for ServiceProviders.
        def self.loadProviders
            @@providerSearchPaths.each do |path|
                dirs = Dir.glob("#{path}/*")
                dirs.each do |dir|
                    if File.directory?( dir ) then
                        if File.file?( "#{dir}/init.rb" ) then
                            require "#{dir}/init.rb"
                        end
                    end
                end
            end
        end

        # List of names of known services.
        #
        # Returns an array with all names of known services.
        def self.serviceNames
            result = []
            @@services.keys.each do |service|
                result.push service.to_s
            end
            return result
        end
        
        # Find service by its name.
        #
        # Returns the ServiceProvider class which is named "name".
        # If no such service exists, nil is returned.
        def self.service( name )
            @@services.keys.each do |service|
                if service.to_s == name then
                    return service
                end
            end
            return nil
        end

        # Loaded service providers.
        #
        # Returns the list of all loaded service providers.
        def self.services
            return @@services.keys
        end

        # Returns the description for service.
        #
        # This will lookup the service description in the internal
        # service/description hash. If the service is not registered, nil is
        # returned.
        def self.serviceDescription( service )
            return @@services[ service ]
        end

        def initialize( parent )
            super( parent )
        end
        
        # The service name.
        #
        # Returns the name of the service account.
        def name
            return ""
        end

        # Configure the service account.
        #
        # Configure the service account (e.g. set username and password)
        # using a dialog.
        #
        # Shall return true if the user finished with accepting the settings,
        # otherwise false.
        def configure
            return false
        end

        # Save settings.
        #
        # Write the settings of the service account to a QByteArray and return
        # that array.
        def saveSettings
            return Qt::ByteArray.new
        end

        # Restore settings.
        #
        # Read settings from data (which is a QByteArray) and apply it to
        # the service account.
        def restoreSettings( data )
        end

        # Update the service.
        #
        # This shall fetch information e.g. from remote servers.
        def update
        end

        # List of issues.
        #
        # Returns a list with all issues provided by the service.
        # Each entry in the list is an instance of the Issue class.
        def issues
            return []
        end

        # Returns the total number of issues.
        def numIssues
            return issues.length
        end

        # Returns the number of unread issues.
        def numUnreadIssues
            result = 0
            issues.each do |issue|
                if issue.isNew then
                    result += 1
                end
            end
            return result
        end
        
    end

end