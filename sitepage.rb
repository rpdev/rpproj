require 'adapters/drupal/idrupal'

require 'Qt4'

class SitePage < Qt::Widget

    slots :fetchIssueList, :refreshView, :updateIssueList

    attr_reader :widget

    attr_reader :username
    attr_reader :password
    attr_reader :site
    attr_reader :name

    attr_reader :adapter

    attr_reader :configureAction
    attr_reader :createIssueAction
    attr_reader :editCurrentAction
    attr_reader :viewCurrentAction

    def initialize( name, parent )
        super parent

        @name = name
        @site = ""
        @username = ""
        @password = ""

        @adapter = IDrupal.new( self )

        connect( @adapter, SIGNAL(:filterUpdated), self, SLOT(:refreshView) )
        connect( @adapter, SIGNAL(:issueListUpdated), self, SLOT(:updateIssueList) )

        createActions
        createUI
        updateFilters
        updateIssueList
    end

    def fetchIssueList
        selectedStatus   =  @statusSelect.itemData( @statusSelect.currentIndex, Qt::UserRole ).toString
        selectedPriority =  @prioritySelect.itemData( @prioritySelect.currentIndex, Qt::UserRole ).toString
        selectedCategory =  @categorySelect.itemData( @categorySelect.currentIndex, Qt::UserRole ).toString
        @adapter.updateIssueList( selectedStatus, selectedPriority, selectedCategory )
    end
    
    def updateIssueList
        @issues.columnCount = 8
        @issues.rowCount = @adapter.issues.size

        @issues.setHorizontalHeaderLabels( ["Project", "Title", "Status",
                                            "Priority", "Category", "Replies",
                                            "Last Updated", "Assigned to"] )

        verticalLabels = []

        for i in (0..@issues.rowCount-1) do
            verticalLabels.push( @adapter.issues[ i ][:node] )
            
            project         = Qt::TableWidgetItem.new( @adapter.issues[ i ][:project] )
            title           = Qt::TableWidgetItem.new( @adapter.issues[ i ][:title] )
            status          = Qt::TableWidgetItem.new( @adapter.issues[ i ][:status] )
            priority        = Qt::TableWidgetItem.new( @adapter.issues[ i ][:priority] )
            category        = Qt::TableWidgetItem.new( @adapter.issues[ i ][:category] )
            replies         = Qt::TableWidgetItem.new( @adapter.issues[ i ][:replies] )
            last_updated    = Qt::TableWidgetItem.new( @adapter.issues[ i ][:last_updated] )
            assigned_to     = Qt::TableWidgetItem.new( @adapter.issues[ i ][:assigned_to] )

            @issues.setItem( i, 0, project )
            @issues.setItem( i, 1, title )
            @issues.setItem( i, 2, status )
            @issues.setItem( i, 3, priority )
            @issues.setItem( i, 4, category )
            @issues.setItem( i, 5, replies )
            @issues.setItem( i, 6, last_updated )
            @issues.setItem( i, 7, assigned_to )

            for j in (0..@issues.columnCount-1) do
                @issues.item(i, j).setFlags( @issues.item( i, j ).flags & (~Qt::ItemIsEditable) )
            end
        end

        @issues.setVerticalHeaderLabels( verticalLabels )
    end

    def refreshView
        updateFilters
    end

    def editSelectedIssue
        row = @issues.currentRow
        if row >= 0 then
            editIssue( @adapter.issues[ row ][ :node ] )
        end
    end

    def viewSelectedIssue
        row = @issues.currentRow
        if row >= 0 then
            @adapter.viewIssueDetails( @adapter.issues[ row ][ :node ] )
        end
    end

    private

    def createActions
        @configureAction = Qt::Action.new( @name, self )
        @configureAction.connect( SIGNAL(:triggered) ) { @adapter.configure }

        @createIssueAction = Qt::Action.new( tr( "Create Issue" ), self )
        @createIssueAction.connect( SIGNAL(:triggered) ) { @adapter.createIssue }

        @editCurrentAction = Qt::Action.new( tr( "Edit Selected Issue" ), self )
        @editCurrentAction.connect( SIGNAL(:triggered) ) { editSelectedIssue }

        @viewCurrentAction = Qt::Action.new( tr( "View" ), self )
        @viewCurrentAction.connect( SIGNAL(:triggered) ) { viewSelectedIssue }
    end
    
    def createUI
        @statusSelect = Qt::ComboBox.new
        @prioritySelect = Qt::ComboBox.new
        @categorySelect = Qt::ComboBox.new

        @updateListButton = Qt::PushButton.new( "Update" )
        connect( @updateListButton, SIGNAL(:clicked), self, SLOT(:fetchIssueList) )

        @filterLayout = Qt::HBoxLayout.new
        @filterLayout.addWidget( Qt::Label.new( "Status:" ) )
        @filterLayout.addWidget( @statusSelect )
        @filterLayout.addWidget( Qt::Label.new( "Priority:" ) )
        @filterLayout.addWidget( @prioritySelect )
        @filterLayout.addWidget( Qt::Label.new( "Category:" ) )
        @filterLayout.addWidget( @categorySelect )
        @filterLayout.addStretch
        @filterLayout.addWidget( @updateListButton )

        @issues = Qt::TableWidget.new

        @mainLayout = Qt::VBoxLayout.new
        @mainLayout.addItem( @filterLayout )
        @mainLayout.addWidget( @issues )

        setLayout( @mainLayout )
    end

    def updateFilters
        @statusSelect.clear
        @adapter.status_filter[ :items ].each do | item |
            @statusSelect.insertItem( @statusSelect.count,
                                      item[ :value ],
                                      Qt::Variant::fromValue( item[ :key ] ) )
            if item[ :key ] == @adapter.status_filter[ :default ] then
                @statusSelect.currentIndex = @statusSelect.count - 1
            end
        end

        @prioritySelect.clear
        @adapter.priority_filter[ :items ].each do | item |
            @prioritySelect.insertItem( @prioritySelect.count,
                                      item[ :value ],
                                      Qt::Variant::fromValue( item[ :key ] ) )
            if item[ :key ] == @adapter.priority_filter[ :default ] then
                @prioritySelect.currentIndex = @prioritySelect.count - 1
            end
        end

        @categorySelect.clear
        @adapter.category_filter[ :items ].each do | item |
            @categorySelect.insertItem( @categorySelect.count,
                                      item[ :value ],
                                      Qt::Variant::fromValue( item[ :key ] ) )
            if item[ :key ] == @adapter.category_filter[ :default ] then
                @categorySelect.currentIndex = @categorySelect.count - 1
            end
        end
    end

    def editIssue( node )
        @adapter.editIssue( node )
    end
    
end