require 'rubygems'
require 'mechanize'
require 'Qt4'

class DrupalQuery

    ExitOk = 0
    ExitLoginFailed = 1
    ExitLogoutFailed = 2
    ExitNoSite = 3
    ExitUnknownQuery = 4
    ExitIssueCreateFailed = 5
    ExitInvalidNode = 6

    QueryIssues = "query_issue_list"
    QueryIssueFilters = "query_issue_filters"
    QueryProjects = "query_project_list"
    QueryIssueCreateOptions = "query_issue_create_options"
    QueryIssueEditOptions = "query_issue_edit_options"
    QueryCreateIssue = "query_create_issue"
    QueryEditIssue = "query_edit_issue"
    QueryIssueDetails = "query_issue_details"

    def initialize
        @query = ""
        @username = ""
        @password = ""
        @site = ""

        @data = nil
        @reader = nil

        @agent = Mechanize.new
    end

    def execute
        readData
        readStdHeader
        case @query
        when QueryIssues                then queryIssueList
        when QueryIssueFilters          then queryIssueFilter
        when QueryProjects              then queryProjectList
        when QueryIssueCreateOptions    then queryIssueCreateOptions
        when QueryIssueEditOptions      then queryIssueEditOptions
        when QueryCreateIssue           then queryCreateIssue
        when QueryEditIssue             then queryEditIssue
        when QueryIssueDetails          then queryIssueDetailedInformation
        else exit ExitUnknownQuery
        end
    end

    # Takes a list representing options in a drop down menu and
    # serialize them into stream.
    def self.serializeSelectList( list, stream )
        stream << "#{list[ :items ].size }"
        list[ :items ].each do | item |
            stream << item[ :key ]
            stream << item[ :value ]
        end
        stream << list[ :default ]
    end

    # This reverses the serializeSelectList operation: It takes a stream and
    # reads a select list from the current position.
    def self.deserializeSelectList( stream )
        result = { :items => [], :default => "" }
        num = ""
        stream >> num
        num = num.to_i
        while num > 0 do
            num -= 1
            key = ""; value = ""
            stream >> key >> value
            result[ :items ].push( { :key => key, :value => value } )
        end
        default = ""
        stream >> default
        result[ :default ] = default
        return result
    end
    
    private

    # Reads the data from STDIN and creates a reader for it.
    def readData
        @data = Qt::ByteArray.new( STDIN.read )
        @reader = Qt::DataStream.new( @data, Qt::IODevice::ReadOnly )
    end

    # Read standard information (as site, login data) from input and store so
    # we can use it.
    def readStdHeader
        @reader >> @query >>
                   @username >>
                   @password >>
                   @site
        if @site.empty? then
            exit ExitNoSite
        end
    end

    # Searches the page for error information. If no is found, nil is returned,
    # otherwise the found error messages. If nil is passed, an error message is
    # generated as well.
    def gatherErrorInfo( page )
        if page.nil? then
            return "NIL page returned"
        end
        search = page.search( "//div[contains(@class,'error')]" )
        msg = nil
        if not search.empty? then
            msg = ""
            search.each do |line|
                msg = line.text + "\n"
            end
        end
        return msg
    end

    # Starts a new session. This MUST be called in every method that requires
    # accessing user-specific pages (i.e. where an login is required).
    def startSession
        page = @agent.get( @site + "/user" )
        if not page.nil? then
            form = findFormById( page, "user-login" )
            if not form.nil? then
                form[ "name" ] = @username
                form[ "pass" ] = @password
                page = @agent.submit( form )
                errors = gatherErrorInfo( page )
                if not errors.nil? then
                    STDERR.write( errors )
                    exit ExitLoginFailed
                end
            end
        end
    end

    # Ends the current running session.
    def stopSession
        page = @agent.get( @site + "/logout" )
        errors = gatherErrorInfo( page )
        if not errors.nil? then
            STDERR.write( errors )
            exit ExitLogoutFailed
        end
    end

    # Fetches the list of projects ( for the create issue form ) and
    # writes it to STDOUT.
    def queryProjectList
        data = Qt::ByteArray.new
        stream = Qt::DataStream.new( data, Qt::IODevice::WriteOnly )
        
        startSession
        page = @agent.get( @site + "/node/add/project-issue" )
        if page then
            form = findFormById( page, "project-issue-pick-project-form" )
            if form then
                DrupalQuery::serializeSelectList(
                    readSelectOptionsById( form.form_node, "edit-pid" ),
                    stream )
            end
        end
        stopSession
        STDOUT.write( data )
        exit ExitOk
    end

    def queryIssueCreateOptions
        data = Qt::ByteArray.new
        stream = Qt::DataStream.new( data, Qt::IODevice::WriteOnly )
        
        startSession
        project = ""
        @reader >> project
        if not project.empty? and not ( project == "0" ) then
            page = @agent.get( @site + "/node/add/project-issue/" + project )
            if page then
                form = findFormById( page, "node-form" )
                if form then
                    DrupalQuery::serializeSelectList( readSelectOptionsById( form.form_node, "edit-component" ), stream )
                    DrupalQuery::serializeSelectList( readSelectOptionsById( form.form_node, "edit-rid" ), stream )
                    DrupalQuery::serializeSelectList( readSelectOptionsById( form.form_node, "edit-category" ), stream )
                    DrupalQuery::serializeSelectList( readSelectOptionsById( form.form_node, "edit-priority" ), stream )
                    DrupalQuery::serializeSelectList( readSelectOptionsById( form.form_node, "edit-assigned" ), stream )
                    DrupalQuery::serializeSelectList( readSelectOptionsById( form.form_node, "edit-sid" ), stream )
                end
            end
        end
        stopSession
        STDOUT.write( data )
        exit DrupalQuery::ExitOk
    end

    def queryIssueEditOptions
        data = Qt::ByteArray.new
        stream = Qt::DataStream.new( data, Qt::IODevice::WriteOnly )
        
        startSession
        node = ""
        @reader >> node
        page = @agent.get( @site + "/comment/reply/" + node )
        if page then
            form = findFormById( page, "comment-form" )
            if form then
                DrupalQuery::serializeSelectList( readSelectOptionsById( form.form_node, "edit-project-info-pid" ), stream )
                DrupalQuery::serializeSelectList( readSelectOptionsById( form.form_node, "edit-project-info-component" ), stream )
                DrupalQuery::serializeSelectList( readSelectOptionsById( form.form_node, "edit-project-info-rid" ), stream )
                DrupalQuery::serializeSelectList( readSelectOptionsById( form.form_node, "edit-category" ), stream )
                DrupalQuery::serializeSelectList( readSelectOptionsById( form.form_node, "edit-priority" ), stream )
                DrupalQuery::serializeSelectList( readSelectOptionsById( form.form_node, "edit-project-info-assigned" ), stream )
                DrupalQuery::serializeSelectList( readSelectOptionsById( form.form_node, "edit-sid" ), stream )
                stream << form[ "title" ]
                stream << form[ "comment" ]
            end
        end
        stopSession
        STDOUT.write( data )
        exit DrupalQuery::ExitOk
    end

    def queryCreateIssue
        startSession
        project = "";
        component = ""
        version = ""
        category = ""
        priority = ""
        assigned = ""
        status = ""
        title = ""
        body = ""
        @reader >> project >> component >> version >> category >> priority >> assigned >>
                   status >> title >> body
        page = @agent.get( @site + "/node/add/project-issue/" + project )
        if page then
            form = findFormById( page, "node-form" )
            if form then
                form[ "component" ] = component
                form[ "rid" ] = version
                form[ "category" ] = category
                form[ "priority" ] = priority
                form[ "assigned" ] = assigned
                form[ "sid" ] = status
                form[ "title" ] = title
                form[ "body" ] = body
                page = @agent.submit( form )
                errors = gatherErrorInfo( page )
                if not errors.nil? then
                    STDERR.write( errors )
                    exit ExitIssueCreateFailed
                end
            end
        end
        stopSession
    end

    def queryEditIssue
        startSession
        node = ""
        component = ""
        version = ""
        category = ""
        priority = ""
        assigned = ""
        status = ""
        title = ""
        body = ""
         @reader >> node >> component >> version >> category >> priority >> assigned >>
                   status >> title >> body
        page = @agent.get( @site + "/comment/reply/" + node )
        if page then
            form = findFormById( page, "comment-form" )
            if form then
                form[ "project_info[component]" ] = component
                form[ "project_info[rid]" ] = version
                form[ "category" ] = category
                form[ "priority" ] = priority
                form[ "project_info[assigned]" ] = assigned
                form[ "sid" ] = status
                form[ "title" ] = title
                form[ "comment" ] = body
                page = @agent.submit( form )
                errors = gatherErrorInfo( page )
                if not errors.nil? then
                    STDERR.write( errors )
                    exit ExitIssueCreateFailed
                end
            end
        end
        stopSession
    end

    def findFormById( page, id )
        page.forms.each do |f|
            if f.form_node.attribute( "id" ) and f.form_node.attribute( "id" ).value == id then
                return f
            end
        end
        return nil
    end

    def readSelectOptionsById( form, id )
        result = { :items => [], :default => "" }
        form.search( "//select[@id='" + id + "']//option" ).each do |option|
            key = option.attribute( "value" ).text
            value = option.text.strip
            result[ :items ].push( { :key => key, :value => value } )
            if option.attribute( "selected" ) and option.attribute( "selected" ).value == "selected" then
                result[ :default ] = key
            end
        end
        return result
    end

    def fetchEditOptions( node )
        options = []
        if beginSession then
            begin
                page = @agent.get( @site + "/comment/reply/" + node )

                form = page.search( "//form[@id='comment-form']" )[ 0 ]

                options.push( {
                    :id => "title",
                    :name => tr( "Title" ),
                    :type => :String,
                    :value => form.search( "//input[@id='edit-title']" ).attribute( "value" ).text
                } )

                component = { :id => "component", :name => tr( "Component" ), :type => :Select, :default => nil, :value => {} }
                component = component.merge( readSelectOptionsById( form, "edit-project-info-component" ) )
                options.push( component )

                assigned = { :id => "assigned", :name => tr( "Assigned To" ), :type => :Select, :default => nil, :value => {} }
                assigned = assigned.merge( readSelectOptionsById( form, "edit-project-info-assigned" ) )
                options.push( assigned )

                category = { :id => "category", :name => tr( "Category" ), :type => :Select, :default => nil, :value => {} }
                category = category.merge( readSelectOptionsById( form, "edit-category" ) )
                options.push( category )

                priority = { :id => "priority", :name => tr( "Priority" ), :type => :Select, :default => nil, :value => {} }
                priority = priority.merge( readSelectOptionsById( form, "edit-priority" ) )
                options.push( priority )

                status = { :id => "priority", :name => tr( "Priority" ), :type => :Select, :default => nil, :value => {} }
                status = status.merge( readSelectOptionsById( form, "edit-sid" ) )
                options.push( status )

                options.push( {
                    :id => "comment",
                    :name => tr( "Comment" ),
                    :type => :Text,
                    :value => ""
                } )
            rescue => e
                rpprojPrintError( tr( "Failed to retrieve edit options" ),
                                  tr( "There was an error while trying to fetch the edit options." ),
                                  e)
                options = []
            end
            endSession
        end
        return options
    end

    def queryIssueList
        data = Qt::ByteArray.new
        stream = Qt::DataStream.new( data, Qt::IODevice::WriteOnly )
        
        startSession
        page = @agent.get( @site + "/project/issues" )
        if page then
            page.search("//table[contains(@class,'project-issue')]/tbody/tr").each do | entry |
                entries = entry.search("td")
                stream << entries[1].search("a")[0].attribute("href").text.match(/\/([0-9]+)$/)[1] << # node
                       entries[0].text.strip << # project
                       entries[1].text.strip << # title
                       entries[2].text.strip << # status
                       entries[3].text.strip << # priority
                       entries[4].text.strip << # category
                       entries[5].text.strip << # replies
                       entries[6].text.strip << # last updated
                       entries[7].text.strip    # assigned to
            end
        end
        stopSession
        STDOUT.write( data )
        exit ExitOk
    end

    def queryIssueFilter
        data = Qt::ByteArray.new
        stream = Qt::DataStream.new( data, Qt::IODevice::WriteOnly )
        
        startSession
        page = @agent.get( @site + "/project/issues" )
        if page then
            form = findFormById( page, "views-exposed-form-project-issue-all-projects-page-1" )
            if form then
                DrupalQuery.serializeSelectList(
                    readSelectOptionsById( form.form_node, "edit-status" ),
                    stream )
                DrupalQuery.serializeSelectList(
                    readSelectOptionsById( form.form_node, "edit-priorities" ),
                    stream )
                DrupalQuery.serializeSelectList(
                    readSelectOptionsById( form.form_node, "edit-categories" ),
                    stream )
            end
        end
        stopSession
        STDOUT.write( data )
        exit DrupalQuery::ExitOk
    end

    def queryIssueDetailedInformation
        data = Qt::ByteArray.new
        stream = Qt::DataStream.new( data, Qt::IODevice::WriteOnly )

        node = ""
        @reader >> node

        if node.empty? then
            exit ExitInvalidNode
        end
        
        startSession
        page = @agent.get( @site + "/node/" + node )
        if page then
            stream << page.search( "//h1[contains(@class,'title')]" )[ 0 ].text
            stream << page.search( "//div[contains(@class,'project-issue')]/p" )[ 0 ].text

            comments = page.search( "//div[contains(@class,'comment')]" )
            stream << "#{comments.size}"

            comments.each do |comment|
                stream << comment.search( "//div[contains(@class,'content')]/p" )[0].text
            end
        end
        stopSession
        STDOUT.write( data )
        exit DrupalQuery::ExitOk
    end

end