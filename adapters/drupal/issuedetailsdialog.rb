require 'Qt4'

class IssueDetailsDialog < Qt::Dialog

    def initialize( parent )
        super( parent )
        createUi
    end

    def setData( data )
        stream = Qt::DataStream.new( data, Qt::IODevice::ReadOnly )

        title = ""
        description = ""
        stream >> title >> description
        @text.append( "<h1>#{title}</h1><p>#{description}</p>" )

        numComments = ""
        stream >> numComments
        numComments = numComments.to_i
        while numComments > 0 do
            numComments -= 1
            text = ""
            stream >> text
            @text.append( "<p>#{text}</p>" )
        end
    end

    private

    def createUi
        @text = Qt::TextEdit.new( self )

        @mainLayout = Qt::VBoxLayout.new( self )
        @mainLayout.addWidget( @text )
        setLayout( @mainLayout )
    end
    
end