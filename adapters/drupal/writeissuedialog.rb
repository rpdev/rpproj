require 'Qt4'

# Dialog for creating and editing issues of a Drupal node.
#
# This dialog is used to create new issues for a Drupal bug tracker or
# to comment a issue (which is equivalent to editing its properties).
class WriteIssueDialog < Qt::Dialog

    signals :projectChanged, :saveIssue, :createIssue
    slots :doAccept, :beginTransaction, :endTransaction, :submitClicked, 'selectedProjectChanged(int)'

    attr_reader :mode

    # In that mode, the dialog is used to create a completely new task.
    CreateIssue = 0

    # In the edit mode the dialog is used to reply to an issue.
    EditIssue   = 1

    # Identifiers for accessing the different lists
    ProjectList     = 0
    VersionList     = 1
    ComponentList   = 2
    CategoryList    = 3
    PriorityList    = 4
    AssignedList    = 5
    StatusList      = 6

    # Create a new dialog.
    #
    # Title is the title of the dialog.
    #
    # Mode is either CreateIssue or EditIssue and determines the look
    # of the dialog.
    def initialize( title, mode )
        super()
        setWindowTitle( title )
        @mode = mode

        createUi
        setupSignals
    end

    # Populates a list with the given data.
    #
    # Populates "list" with "data" (where data is an array if hashes holding
    # :key => anId, :value => anTitle) and preselecting the entry "default"
    def populateList( list, data )
        widget = selectListWidget( list )
        if not widget.nil? then
            setListData( widget, data[ :items ], data[ :default ] )
        end
    end

    # Returns the value of the list identified by "list"
    def listValue( list )
        widget = selectListWidget( list )
        if not widget.nil? then
            return valueFromList( widget )
        end
        return ""
    end

    # Returns the currently set subject
    def subject
        return @issueTitle.text
    end

    # Sets the subject for the issue.
    def subject=( s )
        @issueTitle.text = s
    end

    # Returns the currently set message/comment.
    def message
        return @description.toPlainText
    end

    # Sets the message/comment for the issue.
    def message=( m )
        @description.text = m
    end
    
    ###########
    # Slots
    ###########

    # Called to notify about the start of an transaction. This
    # deactivates the UI so that the transaction can finish before proceeding.
    def beginTransaction
        setEnabled( false )
    end

    # Called to notify about the end of an transaction. This usually
    # reactivates UI parts so that the user can proceed.
    def endTransaction
        setEnabled( true )
    end

    # This gets called when the submit button is hit.
    # Will emit further signals, so that an owner can do
    # further processing.
    def submitClicked
        if listValue( ProjectList ) == "0" then
            Qt::MessageBox::warning( self, tr( "No Project Selected" ),
                                     tr( "You must select a project for the issue!" ) )
            return
        end

        if listValue( ComponentList ) == "0" then
            Qt::MessageBox::warning( self, tr( "No Component Selected" ),
                                     tr( "You must select a component for the issue!" ) )
            return
        end

        if listValue( VersionList ) == "0" then
            Qt::MessageBox::warning( self, tr( "No Version Selected" ),
                                     tr( "You must select a version for the issue!" ) )
            return
        end

        case @mode
        when CreateIssue then emit createIssue
        when EditIssue then emit saveIssue
        end
    end

    # This is called automatically when the user selects a different project. This
    # will emit an signal so that the owning adapter can fetch additional
    # options for the newly selected project.
    def selectedProjectChanged( index )
        emit projectChanged
    end

    private

    # Create the user interface
    def createUi
        @project        = Qt::ComboBox.new( self )
        @version        = Qt::ComboBox.new( self )
        @component      = Qt::ComboBox.new( self )
        @category       = Qt::ComboBox.new( self )
        @priority       = Qt::ComboBox.new( self )
        @assigned       = Qt::ComboBox.new( self )
        @status         = Qt::ComboBox.new( self )
        @issueTitle     = Qt::LineEdit.new( self )
        @description    = Qt::TextEdit.new( self )
        if @mode == EditIssue then
            @project.enabled = false
        end
        
        @componentLayout = Qt::GridLayout.new
        @componentLayout.addWidget( Qt::Label.new( tr( "Project" ), self ),       0, 0 )
        @componentLayout.addWidget( Qt::Label.new( tr( "Version" ), self ),       1, 0 )
        @componentLayout.addWidget( Qt::Label.new( tr( "Component" ), self ),     2, 0 )
        @componentLayout.addWidget( Qt::Label.new( tr( "Category" ), self ),      3, 0 )
        @componentLayout.addWidget( Qt::Label.new( tr( "Priority" ), self ),      4, 0 )
        @componentLayout.addWidget( Qt::Label.new( tr( "Assigned to" ), self ),   5, 0 )
        @componentLayout.addWidget( Qt::Label.new( tr( "Status" ), self ),        6, 0 )
        @componentLayout.addWidget( Qt::Label.new( tr( "Subject" ), self ),       7, 0 )
        @componentLayout.addWidget( Qt::Label.new( tr( "Description" ), self ),   8, 0 )

        @componentLayout.addWidget( @project,       0, 1 )
        @componentLayout.addWidget( @version,       1, 1 )
        @componentLayout.addWidget( @component,     2, 1 )
        @componentLayout.addWidget( @category,      3, 1 )
        @componentLayout.addWidget( @priority,      4, 1 )
        @componentLayout.addWidget( @assigned,      5, 1 )
        @componentLayout.addWidget( @status,        6, 1 )
        @componentLayout.addWidget( @issueTitle,    7, 1 )
        @componentLayout.addWidget( @description,   8, 1 )

        @submitButton = Qt::PushButton.new( tr( "OK" ), self )
        @cancelButton = Qt::PushButton.new( tr( "Cancel" ), self )
        case @mode
        when CreateIssue then @submitButton.text = tr( "Create" )
        when EditIssue then @submitButton.text = tr( "Save" )
        end
        @buttonLayout = Qt::HBoxLayout.new
        @buttonLayout.addWidget( @submitButton )
        @buttonLayout.addWidget( @cancelButton )

        @mainLayout = Qt::VBoxLayout.new
        @mainLayout.addItem( @componentLayout )
        @mainLayout.addItem( @buttonLayout )
        setLayout( @mainLayout )
    end

    # Setup signal/slot connections
    def setupSignals
        connect( @submitButton, SIGNAL(:clicked), self, SLOT(:submitClicked) )
        connect( @cancelButton, SIGNAL(:clicked), self, SLOT(:reject) )
        connect( @project, SIGNAL('currentIndexChanged(int)'), self, SLOT('selectedProjectChanged(int)') )
    end

    # Returns the list associated with the given id.
    def selectListWidget( id )
        case id
        when ProjectList then @project
        when VersionList then @version
        when ComponentList then @component
        when CategoryList then @category
        when PriorityList then @priority
        when AssignedList then @assigned
        when StatusList then @status
        else nil
        end
    end

    # Returns the value currently selected in the given widget (which is
    # a QComboBox).
    def valueFromList( widget )
        v = widget.itemData( widget.currentIndex, Qt::UserRole )
        if v.toString.nil? then
            ""
        else
            v.toString
        end
    end

    # Populates a QComboBox with values, using default as preselected value.
    def setListData( widget, values, default = "" )
        current = valueFromList( widget )
        widget.clear
        hasCurrent = false
        values.each do | entry |
            widget.addItem( entry[ :value ], Qt::Variant::fromValue( entry[ :key ] ) )
            if entry[ :key ] == current then
                hasCurrent = true
            end
        end
        if hasCurrent then
            setActiveItem( widget, current ) # if previously selected, re-set the current value
        else
            setActiveItem( widget, default ) # set default entry always
        end
    end

    # Sets the selected item in a QComboBox to key.
    def setActiveItem( widget, key )
        for i in (0..widget.count-1) do
            v = widget.itemData( i, Qt::UserRole ).toString
            if v == key then
                widget.currentIndex = i
                return
            end
        end
    end
    
end