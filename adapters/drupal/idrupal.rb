require File.dirname( __FILE__ ) + '/writeissuedialog'
require File.dirname( __FILE__ ) + '/issuedetailsdialog'
require File.dirname( __FILE__ ) + '/querymaker'

require 'defaultadapterconfig'

require 'rubygems'
require 'mechanize'

# Interface for Drupal
class IDrupal < Qt::Object

    signals :filterUpdated, :issueListUpdated
    slots :selectedProjectChanged

    # Login credentials and site name
    attr_accessor :username
    attr_accessor :password
    attr_accessor :site

    # Options for filtering
    attr_reader :status_filter
    attr_reader :priority_filter
    attr_reader :category_filter

    # the issue list
    attr_reader :issues

    # Constructor
    def initialize( parent )
        super parent
        
        @site = ""
        @username = ""
        @password = ""

        @status_filter = { :items => [], :default => "" }
        @priority_filter = { :items => [], :default => "" }
        @category_filter = { :items => [], :default => "" }
        
        @issues = []

        @process = Qt::Process.new( self )
        @process.connect( SIGNAL('finished(int,QProcess::ExitStatus)')) { jobFinished }
        @jobQueue = []
        @currentJob = nil
    end

    def scheduleJob( id, data, unique = false )
        if unique then
            if hasJob( id, true ) then
                return
            end
        end
        @jobQueue.push( { :id => id, :data => data } )
        if @currentJob == nil then
            startNextJob
        end
    end

    def hasJob( id, considerCurrent = false )
        if considerCurrent and not @currentJob.nil? and @currentJob[ :id ] == id then
            return true
        end
        @jobQueue.each do | job |
            if job[ :id ] == id then
                return true
            end
        end
        return false
    end

    def updateIssueList( status, priority, category )
        scheduleJob( DrupalQuery::QueryIssues,
                     makeIssueData( [ DrupalQuery::QueryIssues, @username, @password, @site ] ),
                     true )
    end

    def updateIssueFilters
         scheduleJob( DrupalQuery::QueryIssueFilters,
                      makeIssueData( [ DrupalQuery::QueryIssueFilters, @username, @password, @site ] ),
                      true )
    end

    def configure
        dialog = DefaultSiteLoginDialog.new( @site, @username, @password, nil )
        if dialog.exec == Qt::Dialog.Accepted then
            @site = dialog.site
            @username = dialog.username
            @password = dialog.password
            updateIssueFilters
        end
    end

    def saveSettings
        return {
            "site" => @site,
            "username" => @username,
            "password" => @password
        }
    end

    def restoreSettings( hash )
        @site = hash["site"].toString            if hash.include?("site")
        @username = hash["username"].toString    if hash.include?("username")
        @password = hash["password"].toString    if hash.include?("password")
        updateIssueFilters
    end

    def createIssue
        @currentWriteIssueDialog = WriteIssueDialog.new( tr( "Edit Issue" ), WriteIssueDialog::CreateIssue )
        @currentWriteIssueDialog.connect( SIGNAL(:projectChanged) ) {
            scheduleJob( DrupalQuery::QueryIssueCreateOptions,
                     makeIssueData( [ DrupalQuery::QueryIssueCreateOptions,
                                      @username, @password, @site,
                                      @currentWriteIssueDialog.listValue( WriteIssueDialog::ProjectList ) ] ),
                     true )
        }
        @currentWriteIssueDialog.connect( SIGNAL(:createIssue) ) {
            scheduleJob( DrupalQuery::QueryCreateIssue,
                         makeIssueData( [
                                         DrupalQuery::QueryCreateIssue,
                                         @username, @password, @site,
                                         @currentWriteIssueDialog.listValue( WriteIssueDialog::ProjectList ),
                                         @currentWriteIssueDialog.listValue( WriteIssueDialog::ComponentList ),
                                         @currentWriteIssueDialog.listValue( WriteIssueDialog::VersionList ),
                                         @currentWriteIssueDialog.listValue( WriteIssueDialog::CategoryList ),
                                         @currentWriteIssueDialog.listValue( WriteIssueDialog::PriorityList ),
                                         @currentWriteIssueDialog.listValue( WriteIssueDialog::AssignedList ),
                                         @currentWriteIssueDialog.listValue( WriteIssueDialog::StatusList ),
                                         @currentWriteIssueDialog.subject,
                                         @currentWriteIssueDialog.message] ),
                         true )
        }
        @currentEditNode = nil
        scheduleJob( DrupalQuery::QueryProjects,
                     makeIssueData( [ DrupalQuery::QueryProjects, @username, @password, @site ] ),
                     true )
        @currentWriteIssueDialog.exec
    end

    def editIssue( node )
        @currentWriteIssueDialog = WriteIssueDialog.new( tr( "Edit Issue" ), WriteIssueDialog::EditIssue )
        @currentWriteIssueDialog.connect( SIGNAL(:saveIssue) ) {
            scheduleJob( DrupalQuery::QueryEditIssue,
                         makeIssueData( [
                                         DrupalQuery::QueryEditIssue,
                                         @username, @password, @site,
                                         @currentEditNode,
                                         @currentWriteIssueDialog.listValue( WriteIssueDialog::ComponentList ),
                                         @currentWriteIssueDialog.listValue( WriteIssueDialog::VersionList ),
                                         @currentWriteIssueDialog.listValue( WriteIssueDialog::CategoryList ),
                                         @currentWriteIssueDialog.listValue( WriteIssueDialog::PriorityList ),
                                         @currentWriteIssueDialog.listValue( WriteIssueDialog::AssignedList ),
                                         @currentWriteIssueDialog.listValue( WriteIssueDialog::StatusList ),
                                         @currentWriteIssueDialog.subject,
                                         @currentWriteIssueDialog.message] ),
                         true )
        }
        @currentEditNode = node
        @issues.each do | issue |
            if issue[ :node ] == node then
                @currentWriteIssueDialog.subject = issue[ :title ]
            end
        end
        scheduleJob( DrupalQuery::QueryIssueEditOptions,
                     makeIssueData( [ DrupalQuery::QueryIssueEditOptions, @username, @password, @site, node ] ),
                     true )
        @currentWriteIssueDialog.exec
    end

    def viewIssueDetails( node )
        @currentIssueDetailsDialog = IssueDetailsDialog.new( nil )
        scheduleJob( DrupalQuery::QueryIssueDetails,
                     makeIssueData( [ DrupalQuery::QueryIssueDetails,
                                      @username, @password, @site,
                                      node ] ) )
        @currentIssueDetailsDialog.show
    end




    
    private

    def startNextJob
        @currentJob = @jobQueue.shift
        @process.start( "ruby", [ "-KU", File.dirname( __FILE__ ) + "/query" ] )
        @process.write( @currentJob[ :data ] )
        @process.closeWriteChannel
    end

    def jobFinished()
        if @process.exitCode != DrupalQuery::ExitOk then
            rpprojPrintError( tr( "Drupal Query Failed" ),
                              tr( "A query to %1 failed.\nCode: %2\nMessage:\n%3" ).gsub( /%1/, @site ).gsub( /%2/, @process.exitCode.to_s ).gsub( /%3/, "#{@process.readAllStandardError}" )
                            )
        else

            data = @process.readAll
            stream = Qt::DataStream.new( data, Qt::IODevice::ReadOnly )
            
            case @currentJob[ :id ]
            when DrupalQuery::QueryIssues then
                @issues = []
                while not stream.atEnd do
                    node = ""
                    project = ""
                    title = ""
                    status = ""
                    priority = ""
                    category = ""
                    replies = ""
                    last_updated = ""
                    assigned = ""
                    stream  >> node >> project >> title >> status >> priority >> category >> replies >> last_updated >> assigned
                    @issues.push( { :node => node,
                                    :project => project,
                                    :title => title,
                                    :status => status,
                                    :priority => priority,
                                    :category => category,
                                    :replies => replies,
                                    :last_updated => last_updated,
                                    :assigned_to => assigned
                                } )
                end
                emit issueListUpdated()
            when DrupalQuery::QueryIssueFilters then
                @status_filter = DrupalQuery::deserializeSelectList( stream )
                @priority_filter = DrupalQuery::deserializeSelectList( stream )
                @category_filter = DrupalQuery::deserializeSelectList( stream )
                emit filterUpdated
            when DrupalQuery::QueryProjects then
                @currentWriteIssueDialog.populateList( WriteIssueDialog::ProjectList, DrupalQuery::deserializeSelectList( stream ) )
            when DrupalQuery::QueryIssueCreateOptions then
                @currentWriteIssueDialog.populateList( WriteIssueDialog::ComponentList, DrupalQuery::deserializeSelectList( stream ) )
                @currentWriteIssueDialog.populateList( WriteIssueDialog::VersionList, DrupalQuery::deserializeSelectList( stream ) )
                @currentWriteIssueDialog.populateList( WriteIssueDialog::CategoryList, DrupalQuery::deserializeSelectList( stream ) )
                @currentWriteIssueDialog.populateList( WriteIssueDialog::PriorityList, DrupalQuery::deserializeSelectList( stream ) )
                @currentWriteIssueDialog.populateList( WriteIssueDialog::AssignedList, DrupalQuery::deserializeSelectList( stream ) )
                @currentWriteIssueDialog.populateList( WriteIssueDialog::StatusList, DrupalQuery::deserializeSelectList( stream ) )
            when DrupalQuery::QueryIssueEditOptions then
                @currentWriteIssueDialog.populateList( WriteIssueDialog::ProjectList, DrupalQuery::deserializeSelectList( stream ) )
                @currentWriteIssueDialog.populateList( WriteIssueDialog::ComponentList, DrupalQuery::deserializeSelectList( stream ) )
                @currentWriteIssueDialog.populateList( WriteIssueDialog::VersionList, DrupalQuery::deserializeSelectList( stream ) )
                @currentWriteIssueDialog.populateList( WriteIssueDialog::CategoryList, DrupalQuery::deserializeSelectList( stream ) )
                @currentWriteIssueDialog.populateList( WriteIssueDialog::PriorityList, DrupalQuery::deserializeSelectList( stream ) )
                @currentWriteIssueDialog.populateList( WriteIssueDialog::AssignedList, DrupalQuery::deserializeSelectList( stream ) )
                @currentWriteIssueDialog.populateList( WriteIssueDialog::StatusList, DrupalQuery::deserializeSelectList( stream ) )
                title = ""; comment = ""
                stream >> title >> comment
                @currentWriteIssueDialog.subject = title
                @currentWriteIssueDialog.message = comment
            when DrupalQuery::QueryCreateIssue then
                @currentWriteIssueDialog.accept
            when DrupalQuery::QueryEditIssue then
                @currentWriteIssueDialog.accept
            when DrupalQuery::QueryIssueDetails then
                @currentIssueDetailsDialog.setData( data )
            end
        end
        if not @jobQueue.empty? then
            startNextJob
        else
            @currentJob = nil
        end
    end

    def makeIssueData( args )
        data = Qt::ByteArray.new
        stream = Qt::DataStream.new( data, Qt::IODevice::WriteOnly )
        args.each do |arg|
            stream << arg
        end
        return data
    end

    
end