require 'lib/ServiceProvider'
require 'lib/Issue'

require 'Qt4'

module RPProj

    # Provides a view for issues.
    #
    # This class provides a simple, one-dimensional view for issues.
    # It displays the issues of exactly one provider; the provider can be set at
    # runtime by setting the provider proeprty of the class.
    class IssueModel < Qt::AbstractItemModel

        # This is emitted to let clients know the used service has changed
        signals :serviceChanged

        COLUMN_SUBJECT = 0

        NUM_COLUMNS = 1

        attr_reader :service

        def initialize( parent = nil )
            super( parent )
            
            @service = nil
            Application.instance.serviceCollector.connect( SIGNAL('accountListUpdated()') ) { accountListUpdated }
        end

        # Returns the number of rows, i.e. the number of issues.
        def rowCount( index )
            if index.valid? then
                return 0
            else
                if @service.nil? then
                    return 0
                else
                    return @service.issues.length
                end
            end
        end

        # Returns the number of columns (properties per issue)
        def columnCount( index )
            if index.valid? then
                return 0
            else
                return NUM_COLUMNS
            end
        end

        # Create the index for given row and column
        def index( row, column, parent = nil )
            return createIndex( row, column )
        end

        # Returns the parent index for the given one.
        def parent( index )
            return Qt::ModelIndex.new
        end

        # Returns data about the model.
        def data( index, role )
            if not @service.nil? then
                issue = @service.issues[ index.row ]
                if not issue.nil? then
                    case role
                    when Qt::DisplayRole then
                        case index.column
                        when COLUMN_SUBJECT then return issue.subject
                        end
                    when Qt::FontRole then
                        if issue.isNew then
                            font = Qt::Font.new
                            font.bold = true
                            return Qt::Variant.fromValue( font )
                        end
                    end
                end
            end
            return Qt::Variant.new
        end

        # Returns data for the headers of views
        def headerData( section, orientation, role )
            if orientation == Qt::Horizontal then
                if role == Qt::DisplayRole then
                    case section
                    when COLUMN_SUBJECT then return Qt::Variant.fromValue( tr( "Issue" ) )
                    end
                end
            end
            return Qt::Variant.new
        end

        # Sets the service to use as source for issues.
        def service=( service )
            if not @service.nil? then
                @service.disconnect( SIGNAL('issueListUpdated()') )
            end
            @service = service
            if not @service.nil? then
                @service.connect( SIGNAL('issueListUpdated()') ) { issueListUpdated }
            end
            issueListUpdated
            emit serviceChanged
        end

        private

        def issueListUpdated
            emit beginResetModel
            emit endResetModel
        end

        def accountListUpdated
            Application.instance.serviceCollector.services.each do |service|
               if @service == service then
                   return
               end
            end
            @service = nil
            emit serviceChanged()
            issueListUpdated
        end
    end

end