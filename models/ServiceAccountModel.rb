require 'lib/ServiceProvider'
require 'lib/ServiceCollector'

require 'app/Application'


module RPProj

    # Model for displaying the list of accounts.
    #
    # This model can be used when displaying the list of accounts. It
    # shows the name of each account and some additional infos.
    class ServiceAccountModel < Qt::AbstractItemModel

        COLUMN_NAME         = 0
        COLUMN_UNREAD       = 1
        COLUMN_TOTAL        = 2

        NUM_COLUMNS         = 3

        def initialize( parent )
            super( parent )

            @serviceCollector = Application.instance.serviceCollector
            @serviceCollector.connect( SIGNAL('accountListUpdated()') ) { accountListUpdated() }

            @showOnlyNames = false
        end
        
        # Returns the number of entries in the list.
        def rowCount( idx )
            if ( idx.valid? ) then
                return 0
            else
                return @serviceCollector.services.length
            end
        end

        # Returns the number of values per entry.
        def columnCount( idx )
            if idx.valid? then
                return 0
            else
                if @showOnlyNames then
                    return 1
                else
                    return NUM_COLUMNS
                end
            end
        end

        def index( row, column, parent = nil )
            return createIndex( row, column )
        end

        def parent( index )
            return Qt::ModelIndex.new
        end

        # Returns data about the model.
        def data( index, role )
            service = nil
            if index.row >= 0 and index.row < @serviceCollector.services.length then
                service = @serviceCollector.services[ index.row ]
            end
            if not service.nil? then
                if role == Qt::DisplayRole then
                    case index.column
                    when COLUMN_NAME then   return Qt::Variant.fromValue( service.name )
                    when COLUMN_UNREAD then return Qt::Variant.fromValue( service.numUnreadIssues )
                    when COLUMN_TOTAL then  return Qt::Variant.fromValue( service.numIssues )
                    end
                end
            end
            return Qt::Variant.new
        end

        # Return header data about the model
        def headerData( section, orientation, role )
            if orientation == Qt::Horizontal then
                if role == Qt::DisplayRole then
                    case section
                    when COLUMN_NAME then return Qt::Variant.fromValue( tr( "Account" ) )
                    when COLUMN_UNREAD then return Qt::Variant.fromValue( tr( "Unread" ) )
                    when COLUMN_TOTAL then return Qt::Variant.fromValue( tr( "Total" ) )
                    end
                end
            end
            return Qt::Variant.new
        end

        def account( index )
            if index.valid? then
                return @serviceCollector.services[ index.row ]
            end
            return nil
        end

        def showOnlyNames=( son )
            @showOnlyNames = son
            emit beginResetModel
            emit endResetModel
        end

        private

        # The account list has changed.
        def accountListUpdated
            emit beginResetModel()
            emit endResetModel()
        end
        
    end

end