require 'lib/ServiceProvider'

require 'Qt4'

module RPProj

    class ProviderModel < Qt::AbstractItemModel

        COLUMN_SERVICE_DESCRIPTION  = 0

        NUM_COLUMNS                 = 1

        
        def initialize( parent = nil )
            super( parent )
        end

        def rowCount( index )
            if index.valid? then
                return 0
            else
                ServiceProvider.services.length
            end
        end

        def columnCount( index )
            if index.valid? then
                return 0
            else
                return NUM_COLUMNS
            end
        end

        def index( row, column, parent )
            return createIndex( row, column )
        end

        def parent( index )
            return Qt::ModelIndex.new
        end

        def data( index, role )
            if index.valid? then
                provider = ServiceProvider.services[ index.row ]
                case role
                when Qt::DisplayRole then
                    case index.column
                    when COLUMN_SERVICE_DESCRIPTION then return Qt::Variant.fromValue( ServiceProvider.serviceDescription( provider ) )
                    end
                end
            end
            return Qt::Variant.new
        end

        def headerData( section, orientation, role )
            if orientation == Qt::Horizontal then
                case role
                when Qt::DisplayRole then
                    case section
                    when COLUMN_SERVICE_DESCRIPTION then return Qt::Variant.fromValue( tr( "Service Description" ) )
                    end
                end
            end
            return Qt::Variant.new
        end

        def provider( index )
            if index.valid? then
                return ServiceProvider.services[ index.row ]
            end
            return nil
        end

    end

end