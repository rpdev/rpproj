#!/usr/bin/ruby -KU
# encoding: utf-8

$LOAD_PATH << File.dirname( __FILE__ )

module RPProj

    # Icon path.
    #
    # Points to the application's icon directory.
    ICON_PATH = "#{File.dirname( __FILE__ )}/gfx/icons/"

end

require 'app/Application.rb'

require 'Qt4'

Qt::CoreApplication::setOrganizationName("RPdev");
Qt::CoreApplication::setOrganizationDomain("rpdev.net");
Qt::CoreApplication::setApplicationName("RPProj")

app = RPProj::Application.new( ARGV )
exit app.exec
