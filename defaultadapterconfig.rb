require 'Qt4'

class DefaultSiteLoginDialog < Qt::Dialog

    def initialize( site, user, pass, parent )
        super parent

        setWindowTitle( tr( "Login" ) )

        @site = Qt::LineEdit.new( self )
        @user = Qt::LineEdit.new( self )
        @pass = Qt::LineEdit.new( self )
        @pass.echoMode = Qt::LineEdit::Password

        @site.text = site
        @user.text = user
        @pass.text = pass

        @login = Qt::PushButton.new( tr( "OK" ) )
        @cancel = Qt::PushButton.new( tr( "Cancel" ) )

        @login.connect(SIGNAL(:clicked)) { self.accept }
        @cancel.connect(SIGNAL(:clicked)) { self.reject }
        @bottomLayout = Qt::HBoxLayout.new
        @bottomLayout.addStretch
        @bottomLayout.addWidget( @login )
        @bottomLayout.addWidget( @cancel )

        @mainLayout = Qt::VBoxLayout.new
        @mainLayout.addWidget( Qt::Label.new( tr( "Website:" ) ) )
        @mainLayout.addWidget( @site )
        @mainLayout.addWidget( Qt::Label.new( tr( "Username:" ) ) )
        @mainLayout.addWidget( @user )
        @mainLayout.addWidget( Qt::Label.new( tr( "Password:" ) ) )
        @mainLayout.addWidget( @pass )
        @mainLayout.addItem( @bottomLayout )

        setLayout( @mainLayout )
    end

    def site
        return @site.text.strip
    end
    
    def username
        return @user.text
    end

    def password
        return @pass.text
    end

end