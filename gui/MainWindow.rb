require 'gui/ServiceConfigurationDialog'
require 'gui/ServiceProviderSelectionDialog'

require 'models/IssueModel'
require 'models/ServiceAccountModel'

require 'app/Application'

require 'Qt4'
require 'qtwebkit'

module RPProj

    # Main window class.
    #
    # This class provides the main window for RPPproj, giving
    # access to general management functions.
    class MainWindow < Qt::MainWindow

        # Menus
        attr_reader :fileMenu

        # Actions
        attr_reader :configureServicesAction
        attr_reader :createNewAccountAction
        attr_reader :quitAction

        # Misc
        attr_reader :trayIcon

        def initialize
            super

            setWindowTitle( tr( "RPProj - Project Management Tool" ) )

            createMenus
            createActions
            createGUI
            setupMenus
            setupActions
            setupGUI
            createTrayIcon

            Application.instance.connect( SIGNAL('aboutToQuit()') ) { writeSettings }
        end

        private

        APPLICATION_ICON = "#{ICON_PATH}rpproj.png"

        def createMenus
            @fileMenu = menuBar.addMenu( tr( "File" ) )
        end

        def createActions
            @configureServicesAction = Qt::Action.new( tr( "Configure Services..." ), self )
            
            @createNewAccountAction = Qt::Action.new( tr( "New Account" ), self )
            
            @quitAction = Qt::Action.new( tr( "Quit" ), self )
            @quitAction.shortcut = tr( "Ctrl+Q" )
        end

        def createGUI
            @mainSplitter = Qt::Splitter.new
            @mainSplitter.orientation = Qt::Horizontal
            
            @secondarySplitter = Qt::Splitter.new
            @secondarySplitter.orientation = Qt::Vertical

            @accountView = Qt::TreeView.new
            @accountModel = ServiceAccountModel.new( self )
            @accountView.model = @accountModel

            @issueView = Qt::TreeView.new
            @issueModel = IssueModel.new( self )
            @issueView.model = @issueModel
            @issueView.visible = false

            @issueDisplay = Qt::WebView.new

            @secondarySplitter.addWidget( @issueView )
            @secondarySplitter.addWidget( @issueDisplay )
            

            @mainSplitter.addWidget( @accountView )
            @mainSplitter.addWidget( @secondarySplitter )

            setCentralWidget( @mainSplitter )
        end

        def setupMenus
            @fileMenu.addAction( @createNewAccountAction )
            @fileMenu.addAction( @configureServicesAction )
            @fileMenu.addAction( @quitAction )
        end
        
        def setupActions
            @configureServicesAction.connect( SIGNAL('triggered()') ) { configureServices }
            @createNewAccountAction.connect( SIGNAL('triggered()') ) { createNewAccount }
            @quitAction.connect( SIGNAL('triggered()') ) { Qt::Application.instance.quit }
        end

        def setupGUI
            @accountView.connect( SIGNAL('clicked(QModelIndex)') ) { |idx| serviceClicked( idx ) }
            @issueModel.connect( SIGNAL('serviceChanged()') ) { issueModelServiceChanged }
        end

        def createTrayIcon
            @trayIcon = Qt::SystemTrayIcon.new( self )
            
            @trayIcon.setIcon( Qt::Icon.new( APPLICATION_ICON ) )

            @trayIconMenu = Qt::Menu.new( tr( "RPProj" ) )
            @trayIconMenu.addAction( @quitAction )
            @trayIcon.setContextMenu( @trayIconMenu )

            @trayIcon.connect( SIGNAL('activated(QSystemTrayIcon::ActivationReason)') ) { toggleMainWindowVisibility }
            
            @trayIcon.show
        end

        # private slots

        def configureServices
            d = ServiceConfigurationDialog.new
            d.exec
        end
        
        def createNewAccount
            d = ServiceProviderSelectionDialog.new( self )
            if d.exec == Qt::Dialog::Accepted then
               provider = d.provider
               if not provider.nil? then
                   meta = ServiceMeta.new
                   meta.name = d.serviceTitle
                   service = provider.new( meta )
                   service.configure
                   meta.service = service
                   Qt::Application.instance.serviceCollector.addService( meta )
               end
            end
        end
        
        def toggleMainWindowVisibility
            if visible? and activeWindow? then
                hide
            else
                show
                activateWindow
            end
        end

        def writeSettings
            settings = Qt::Settings.new
            settings.setValue( Application::SETTINGS_MAINWINDOW_GEOMETRY, Qt::Variant.fromValue( saveGeometry ) )
            settings.setValue( Application::SETTINGS_MAINWINDOW_STATE, Qt::Variant.fromValue( saveState ) )
            settings.sync
        end

        def serviceClicked( index )
            service = Application.instance.serviceCollector.services[ index.row ]
            @issueModel.service = service
        end

        def issueModelServiceChanged
            if @issueModel.service.nil? then
                @issueView.visible = false
            else
                @issueView.visible = true
            end
        end

    end

end