require 'Qt4'

require 'lib/ServiceProvider'

require 'models/ProviderModel'

module RPProj

    # Provides a dialog for selecting a service provider.
    #
    # This will display a modal dialog to the user where
    # he can select one service provider. The selected provider
    # can then be used to create a new account.
    class ServiceProviderSelectionDialog < Qt::Dialog

        def initialize( parent = nil )
            super( parent )

            setupUi
            setupActions
            
            setWindowTitle( tr( "Select a Service" ) )
            resize( 400, 300 )
        end

        # Returns the user set name for the service
        #
        # Returns the name used to display the title.
        # This name can be changed by the user and is
        # only there to give the user a (visual) property to
        # distinguish the service instances from each other.
        def serviceTitle
            return @serviceTitle.text
        end
        
        # Selected provider.
        #
        # Returns the selected service provider or nil, if no provider is available.
        def provider
            return @providerModel.provider( @providerList.currentIndex )
        end

        private

        def setupUi
            @mainLayout = Qt::VBoxLayout.new( self )
            layout = @mainLayout

            @serviceTitle = Qt::LineEdit.new( self )
            @serviceTitle.text = tr( "New Service" )

            @providerModel = ProviderModel.new( self )
            @providerList = Qt::TreeView.new( self )
            @providerList.model = @providerModel

            @buttons = Qt::DialogButtonBox.new( Qt::DialogButtonBox::Ok |
                                               Qt::DialogButtonBox::Cancel,
                                               Qt::Horizontal,
                                               self)
            
            @mainLayout.addWidget( @serviceTitle )
            @mainLayout.addWidget( @providerList )
            @mainLayout.addWidget( @buttons )
        end

        def setupActions
            @buttons.connect( SIGNAL('accepted()') ) { accept }
            @buttons.connect( SIGNAL('rejected()') ) { reject }
        end

    end

end