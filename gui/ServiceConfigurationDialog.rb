require 'Qt4'

module RPProj
    
    class ServiceConfigurationDialog < Qt::Dialog
        
        def initialize( parent = nil )
            super( parent )
            windowTitle = tr( "Configure Services" )
            setupUi
        end
        
        private
        
        def setupUi
            @accountModel = ServiceAccountModel.new( self )
            @accountModel.showOnlyNames = true

            @accountList = Qt::TreeView.new( self )
            @accountList.model = @accountModel
            
            @configureButton = Qt::PushButton.new( self )
            @configureButton.text = tr( "Configure" )
            @configureButton.connect( SIGNAL('clicked()') ) { configureService }

            @renameButton = Qt::PushButton.new( self )
            @renameButton.text = tr( "Rename" )
            @renameButton.connect( SIGNAL('clicked()') ) { renameService }

            @deleteButton = Qt::PushButton.new( self )
            @deleteButton.text = tr( "Remove" )
            @deleteButton.connect( SIGNAL('clicked()') ) { deleteService }
            
            @buttons = Qt::DialogButtonBox.new( Qt::DialogButtonBox::Ok,
                                                Qt::Horizontal,
                                                self )
            @buttons.addButton( @configureButton, Qt::DialogButtonBox::ActionRole )
            @buttons.addButton( @renameButton, Qt::DialogButtonBox::ActionRole )
            @buttons.addButton( @deleteButton, Qt::DialogButtonBox::ActionRole )
            @buttons.connect( SIGNAL('accepted()') ) { accept }
            @buttons.connect( SIGNAL('rejected()') ) { reject }
            
            @mainLayout = Qt::VBoxLayout.new( self )
            @mainLayout.addWidget( @accountList )
            @mainLayout.addWidget( @buttons )
            layout = @mainLayout
        end
        
        # private slots
        
        def configureService
            service = @accountModel.account( @accountList.currentIndex )
            service.configure unless service.nil?
        end
        
        def renameService
            service = @accountModel.account( @accountList.currentIndex )
            if not service.nil? then
                newName = Qt::InputDialog.getText( self,
                                                   tr( "Rename" ),
                                                   tr( "Please enter a new name for the account:" ),
                                                   Qt::LineEdit.Normal,
                                                   service.name )
                if not newName.nil? and not newName.empty? then
                    service.name = newName
                    @accountList.reset
                end
            end
        end

        def deleteService
            service = @accountModel.account( @accountList.currentIndex )
            if not service.nil? then
                if Qt::MessageBox.question( self,
                                            tr( "Remove Account?" ),
                                            tr( "Do you really want to remove this account? Note, that this cannot be undone!" ),
                                            Qt::MessageBox.Yes, Qt::MessageBox.No ) == Qt::MessageBox.Yes then
                    Application.instance.serviceCollector.removeService( service )
                end
            end
        end
        
    end
    
end