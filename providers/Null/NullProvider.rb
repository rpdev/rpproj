require 'Qt4'

require File.dirname( __FILE__ ) + '/NullIssue'

module RPProj

    module Providers

        class NullProvider < ServiceProvider

            def name
                return tr( "Null" )
            end

            def configure
                msg = Qt::MessageBox.new
                msg.windowTitle = tr( "Null Provider Configuration" )
                msg.text = tr( "The Null service does not have any configuration." )
                msg.exec
            end

            def saveSettings
                return Qt::ByteArray.new
            end

            def restoreSettings( data )
            end

            def update
                issue = NullIssue.new( tr( "Test" ), true, {} )
                if @issues.nil? then
                    @issues = []
                end
                @issues.push( issue )
                emit issueListUpdated()
            end

            def issues
                if @issues.nil? then
                    @issues = []
                end
                return @issues
            end

        end

    end

end