require 'Qt4'

module RPProj

    module Providers

        class NullIssue < RPProj::Issue

            def initialize( subject, new, properties )
                @subject = subject
                @isNew = new
                @properties = properties
            end

            def subject
                return @subject
            end

            def isNew
                return @isNew
            end

            def properties
                return @properties
            end

            def showDetails
                @isNew = false
                msg = Qt::MessageBox.new
                msg.windowTitle = Qt::Object::tr( "Null Issue Details" )
                msg.text = Qt::Object::tr( "This is a NullIssue. It does not provide any real information." )
                msg.exec
            end

        end
        
    end

end