require 'editissuedialog'

require 'Qt4'

class EditIssueDialog < Qt::Dialog

    attr_reader :arguments
    attr_reader :results

    slots :writeBackResults
    
    def initialize( title, arguments, parent = nil )
        super( parent )
        setWindowTitle( title )

        @arguments = arguments
        @results = []

        createUI
    end

    def createUI
        @argumentsLayout = Qt::GridLayout.new
        row = 0
        @arguments.each do |argument|
            resultsEntry = { :id => argument[:id], :value => nil, :type => :unknown }

            label = Qt::Label.new( argument[:name] )
            label.alignment = Qt::AlignLeft | Qt::AlignTop
            @argumentsLayout.addWidget( label, row, 0 )

            widget = nil
            if argument[:type].equal?( :String ) then
                resultsEntry[:type] = :String
                widget = Qt::LineEdit.new( self )
                widget.text = argument[:value]
            elsif argument[:type].equal?( :Text ) then
                resultsEntry[:type] = :Text
                widget = Qt::TextEdit.new( self )
                widget.plainText = argument[:value]
            elsif argument[:type].equal?( :Select ) then
                resultsEntry[:type] = :Select
                widget = Qt::ComboBox.new( self )
                argument[:value].keys.each do |key|
                    widget.addItem( argument[:value][key], Qt::Variant::fromValue( key ) )
                    if key == argument[:default] then
                        widget.currentIndex = widget.count - 1
                    end
                end
            end
            if not widget.nil? then
                resultsEntry[:widget] = widget
                @argumentsLayout.addWidget( widget, row, 1 )
            end
            @results.push( resultsEntry )
            
            row += 1
        end

        @submitButton = Qt::PushButton.new( tr( "Submit" ), self )
        connect( @submitButton, SIGNAL(:clicked), self, SLOT(:writeBackResults) )
        
        @cancelButton = Qt::PushButton.new( tr( "Cancel" ), self )
        connect( @cancelButton, SIGNAL(:clicked), self, SLOT(:reject) )
        
        @buttonsLayout = Qt::HBoxLayout.new
        @buttonsLayout.addStretch
        @buttonsLayout.addWidget( @submitButton )
        @buttonsLayout.addWidget( @cancelButton )

        @mainLayout = Qt::VBoxLayout.new
        @mainLayout.addItem( @argumentsLayout )
        @mainLayout.addItem( @buttonsLayout )
        setLayout( @mainLayout )
    end

    def writeBackResults
        @results.each do |result|
            if result[ :type ].equal?( :String ) then
                result[ :value ] = result[ :widget ].text
            elsif result[ :type ].equal?( :Text ) then
                result[ :value ] = result[ :widget ].toPlainText
            elsif result[ :type ].equal?( :Select ) then
                index = result[ :widget ].currentIndex
                result[ :value ] = result[ :widget ].itemData( index, Qt::UserRole ).toString
            end
            result[ :widget ] = nil
        end
        accept
    end
    
end