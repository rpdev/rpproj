require 'sitepage'

require 'Qt4'

class MainWindow < Qt::MainWindow

    attr_reader :sites
    attr_reader :trayIcon
    
    def initialize( parent )
        super( parent )

        @sites = Array.new
        
        setWindowTitle( tr( "RPProj" ) )
        createActions
        createMenus
        createUI
        createTrayIcon
        loadSettings
    end

    def addSite( sitepage )
        @sites.push( sitepage )
        @pages.addTab( sitepage, sitepage.name )
        @settingsMenu.addAction( sitepage.configureAction )
        sitepage.refreshView
    end

    def closeEvent( event )
        saveSettings
        super event
    end

    def toggleVisibility
        if isVisible then
            hide
        else
            show
        end
    end

    def trayIconActivated( reason )
        if reason == Qt::SystemTrayIcon.Trigger then
            toggleVisibility
        end
    end

    def createAccount
        name = ""
        while name.empty? or accountExists( name ) do
            name = Qt::InputDialog::getText( self, tr( "Select Name" ),
                                             tr( "Please input a name for the new account:") )
            if name.nil? then
                return
            end
        end
        addSite( SitePage.new( name, self ) )
    end

    def accountExists( name )
        @sites.each do |site|
            if site.name.equal?( name ) then
                return true
            end
        end
        return false
    end

    def saveAndQuit
        saveSettings
        Qt::Application.instance.quit
    end

    private

    def createActions
        @addAccountAction = Qt::Action.new( tr( "New Account" ), self )
        @addAccountAction.connect( SIGNAL(:triggered) ) { createAccount }

        @createIssueAction = Qt::Action.new( tr( "Create Issue" ), self )
        @createIssueAction.shortcut = 'Ctrl+N'
        @createIssueAction.connect( SIGNAL(:triggered) ) { createNewIssue }

        @editCurrentIssueAction = Qt::Action.new( tr( "Edit Selected Issue" ), self )
        @editCurrentIssueAction.shortcut = 'Ctrl+E'
        @editCurrentIssueAction.connect( SIGNAL(:triggered) ) { editCurrentIssue }

        @viewCurrentIssueAction = Qt::Action.new( tr( "View Issue Details" ), self )
        @viewCurrentIssueAction.shortcut = 'Enter'
        @viewCurrentIssueAction.connect( SIGNAL(:triggered) ) { viewCurrentIssue }
        
        @quitAppAction = Qt::Action.new( tr( "Quit" ), self )
        @quitAppAction.shortcut = 'Ctrl+Q'
        @quitAppAction.connect( SIGNAL(:triggered) ) { saveAndQuit }
    end

    def createMenus
        @fileMenu = Qt::Menu.new( tr( "File" ), self )
        @fileMenu.addAction( @addAccountAction )
        @fileMenu.addSeparator
        @fileMenu.addAction( @viewCurrentIssueAction )
        @fileMenu.addSeparator
        @fileMenu.addAction( @createIssueAction )
        @fileMenu.addAction( @editCurrentIssueAction )
        @fileMenu.addSeparator
        @fileMenu.addAction( @quitAppAction )

        @settingsMenu = Qt::Menu.new( tr( "Settings" ), self )

        menuBar.addMenu( @fileMenu )
        menuBar.addMenu( @settingsMenu )

        @trayMenu = Qt::Menu.new( self )
        @trayMenu.addAction( @quitAppAction )
    end

    def createUI
        @pages = Qt::TabWidget.new( self )

        @mainWidget = Qt::Widget.new( self )
        @mainLayout = Qt::HBoxLayout.new
        @mainLayout.addWidget( @pages )
        @mainWidget.setLayout( @mainLayout )
        setCentralWidget( @mainWidget )
    end

    def createTrayIcon
        @trayIcon = Qt::SystemTrayIcon.new( Qt::Icon.new( Qt::Application.instance.windowIcon ) )
        @trayIcon.setToolTip( tr( "RPProj - Project Management Client" ) )
        @trayIcon.connect( SIGNAL('activated(QSystemTrayIcon::ActivationReason)') ) do |r|
            trayIconActivated(r)
        end
        @trayIcon.contextMenu = @trayMenu
        @trayIcon.show
    end

    def saveSettings
        settings = Qt::Settings.new

        settings.beginGroup( "MainWindow" )
        settings.setValue( "geometry", Qt::Variant::fromValue( saveGeometry ) )
        settings.setValue( "state", Qt::Variant::fromValue( saveState.to_s ) )
        settings.endGroup

        settings.beginGroup( "Accounts" )
        @sites.each do |page|
            settings.beginGroup( page.name )
            settings.remove( "" )
            s = page.adapter.saveSettings
            s.keys.each do |k|
                settings.setValue( k, Qt::Variant::fromValue( s[k] ) )
            end
            settings.endGroup
        end
        settings.endGroup
        
        settings.sync
    end

    def loadSettings
        settings = Qt::Settings.new

        settings.beginGroup( "MainWindow" )
         restoreGeometry( settings.value( "geometry" ).toByteArray )
         restoreState( settings.value( "state" ).toByteArray )
        settings.endGroup

        settings.beginGroup( "Accounts" )
        settings.childGroups.each do |acc|
            site = SitePage.new( acc, self )
            s = {}
            settings.beginGroup( acc )
            settings.childKeys.each do |k|
                s[k] = settings.value( k )
            end
            settings.endGroup
            site.adapter.restoreSettings( s )
            addSite( site )
        end
        settings.endGroup
    end

    def createNewIssue
        if not @pages.currentWidget.nil? then
            @pages.currentWidget.createIssueAction.trigger
        end
    end
    
    def editCurrentIssue
        if not @pages.currentWidget.nil? then
            @pages.currentWidget.editCurrentAction.trigger
        end
    end

    def viewCurrentIssue
        if not @pages.currentWidget.nil? then
            @pages.currentWidget.viewCurrentAction.trigger
        end
    end
    
end
