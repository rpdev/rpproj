require 'Qt4'

require 'gui/MainWindow'

require 'lib/ServiceProvider'
require 'lib/ServiceCollector'

module RPProj

    # Application object
    class Application < Qt::Application
        
        attr_reader :serviceCollector

        SETTINGS_MAINWINDOW_GEOMETRY    = "mainWindowGeometry"
        SETTINGS_MAINWINDOW_STATE       = "mainWindowState"
        SETTINGS_SERVICE_DATA           = "serviceData"
        
        def exec

            ServiceProvider.loadProviders
            
            setQuitOnLastWindowClosed( false )
            setWindowIcon( Qt::Icon.new( APPLICATION_ICON ) )

            @serviceCollector = ServiceCollector.new( self )
            @mainWindow = MainWindow.new

            settings = Qt::Settings.new
            @mainWindow.restoreGeometry( settings.value( SETTINGS_MAINWINDOW_GEOMETRY, Qt::Variant.fromValue( Qt::ByteArray.new ) ).toByteArray )
            @mainWindow.restoreState( settings.value( SETTINGS_MAINWINDOW_STATE, Qt::Variant.fromValue( Qt::ByteArray.new ) ).toByteArray )
            @serviceCollector.restoreSettings( settings.value( SETTINGS_SERVICE_DATA, Qt::Variant.fromValue( Qt::ByteArray.new ) ).toByteArray )
            
            @mainWindow.show

            result = super()
            
            settings.setValue( SETTINGS_SERVICE_DATA, Qt::Variant.fromValue( @serviceCollector.saveSettings ) )
            settings.sync

            return result
        end

        private

        APPLICATION_ICON = "#{ICON_PATH}rpproj.rb"

    end

end